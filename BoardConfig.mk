# Copyright (C) 2013 OmniROM Project
# Copyright (C) 2012 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# inherit from the proprietary version
-include vendor/yuandao/n70s/BoardConfigVendor.mk

# Platform
TARGET_BOOTLOADER_BOARD_NAME := rk
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true

# Architecture
TARGET_ARCH := arm
TARGET_BOARD_PLATFORM := rk
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_VARIANT := cortex-a9
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_ARCH_VARIANT_CPU := cortex-a9
TARGET_ARCH_VARIANT_FPU := neon
ARCH_ARM_HAVE_NEON := true
ARCH_ARM_HAVE_VFP := true
TARGET_CPU_SMP := true
ARCH_ARM_HAVE_TLS_REGISTER := true
TARGET_GLOBAL_CFLAGS += -mtune=cortex-a9 -mfpu=neon -mfloat-abi=softfp
TARGET_GLOBAL_CPPFLAGS += -mtune=cortex-a9 -mfpu=neon -mfloat-abi=softfp

# Partitions
BOARD_BOOTIMAGE_PARTITION_SIZE := 16777216
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16777216
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 536870912
BOARD_USERDATAIMAGE_PARTITION_SIZE := 2147483648
BOARD_KERNELIMAGE_PARTITION_SIZE := 8388608
BOARD_FLASH_BLOCK_SIZE := 4096

# Kernel
BOARD_KERNEL_CMDLINE := 
BOARD_KERNEL_BASE := 0x60400000
BOARD_PAGE_SIZE := 16384
TARGET_PREBUILT_KERNEL := device/yuandao/n70s/kernel
BOARD_EGL_CFG := device/yuandao/n70s/egl.cfg

# Recovery
BOARD_HAS_NO_SELECT_BUTTON := true
TARGET_PREBUILT_RECOVERY_KERNEL := device/yuandao/n70s/kernel
TARGET_RECOVERY_INITRC := device/yuandao/n70s/recovery/init.rc
TARGET_RECOVERY_FSTAB := device/yuandao/n70s/recovery/recovery.fstab
BOARD_HAS_LARGE_FILESYSTEM := true
TARGET_USE_CUSTOM_LUN_FILE_PATH := "/sys/devices/platform/mt_usb/gadget/lun%d/file"
BOARD_UMS_LUNFILE := "/sys/class/android_usb/android0/f_mass_storage/lun/file"
BOARD_UMS_2ND_LUNFILE := "/sys/class/android_usb/android0/f_mass_storage/lun1/file"
BOARD_USE_CUSTOM_RECOVERY_FONT:= \"roboto_15x24.h\"
DEVICE_RESOLUTION := 1024x600
TARGET_RECOVERY_PIXEL_FORMAT := "RGB_565"
#TARGET_RECOVERY_PIXEL_FORMAT := "BGRA_8888"
#TARGET_RECOVERY_PIXEL_FORMAT := "RGBX_8888"

# Misc
USE_OPENGL_RENDERER := true
BOARD_USE_SKIA_LCDTEXT := true
BOARD_USES_HWCOMPOSER := true
BOARD_EGL_NEEDS_LEGACY_FB := true
ENABLE_WEBGL := true
BOARD_NEEDS_MEMORYHEAPPMEM := true
TARGET_USES_ION := true
TARGET_FORCE_CPU_UPLOAD := true

# Bluetooth
BOARD_HAVE_BLUETOOTH := false
BOARD_HAVE_BLUETOOTH_CSR := false

#nand
PRODUCT_COPY_FILES += \
    device/yuandao/n70s/recovery/init.recovery.rk30board.rc:root/init.recovery.rk30board.rc \
    device/yuandao/n70s/recovery/rk29-ipp.ko.3.0.8+:root/rk29-ipp.ko.3.0.8+ \
    device/yuandao/n70s/recovery/rk30xxnand_ko.ko.3.0.8+:root/rk30xxnand_ko.ko.3.0.8+ \
    device/yuandao/n70s/recovery/rk30xxnand_ko.ko.3.0.36+:root/rk30xxnand_ko.ko.3.0.36+ \
    device/yuandao/n70s/recovery/rk29-ipp.ko.3.0.36+:root/rk29-ipp.ko.3.0.36+

#twrp
BOARD_CUSTOM_RECOVERY_KEYMAPPING := device/yuandao/n70s/recovery/recovery_keys.c
#TW_BOARD_CUSTOM_GRAPHICS := ../../../device/yuandao/n70s/recovery/graphics.c
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_INTERNAL_STORAGE_PATH := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "sdcard"
TW_EXTERNAL_STORAGE_PATH := "/external_sd"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "external_sd"
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_NO_SCREEN_TIMEOUT := true
TW_NO_SCREEN_BLANK := true
TW_FORCE_CPUINFO_FOR_DEVICE_ID := false
TW_USE_MODEL_HARDWARE_ID_FOR_DEVICE_ID := true
TARGET_USERIMAGES_USE_EXT4 := true
TWHAVE_SELINUX := true
TW_NO_BATT_PERCENT := true
#TW_NO_USB_STORAGE := true
TW_INCLUDE_FB2PNG := true
#TW_ALWAYS_RMRF := true
#BOARD_HAS_FLIPPED_SCREEN := true
#RECOVERY_TOUCHSCREEN_FLIP_Y := true
#RECOVERY_TOUCHSCREEN_FLIP_X := true
#RECOVERY_SDCARD_ON_DATA := true
#TWRP_EVENT_LOGGING := true
#BOARD_HAS_NO_REAL_SDCARD := true
#TW_IGNORE_MAJOR_AXIS_0 := true

BOARD_CUSTOM_BOOTIMG_MK := device/yuandao/n70s/custombootimg.mk
